﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorTelas : MonoBehaviour {

    public enum Screens {
        Game,Menu,SplashScreen, Tela_Load
    }
    public Screens primeira_Screen;
    public static Screens screen = Screens.Game;

    private static Screens actualScreen;
    private static Screens lastScreen;

    [Space(20)]
    [SerializeField]
    GameObject game, menu, splashScreen, tela_Load;


    public static GameObject _game, _menu, _splashScreen, _tela_Load;

    private Animator splash_Anim;

    [Space(20)]
    public GameObject cam_Menu_Splash;
    public static GameObject cam;

    void Awake () {
        _game = game;
        _menu = menu;
        _splashScreen = splashScreen;
        _tela_Load = tela_Load;
        cam = cam_Menu_Splash;

        _game.SetActive(false);
        _menu.SetActive(false);
        _splashScreen.SetActive(false);
        _tela_Load.SetActive(false);
        cam.SetActive(false);

        lastScreen = primeira_Screen;
        ChangeScreen(primeira_Screen);

        splash_Anim = _splashScreen.transform.GetChild(0).GetComponent<Animator>();
    }

    void Update() {
        if(lastScreen == Screens.SplashScreen && splash_Anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            ChangeScreen(Screens.Menu);
        }
    }

    public static void ChangeScreen(Screens newScreen)
    {
        switch (lastScreen)
        {
            case Screens.SplashScreen:
                _splashScreen.SetActive(false);
                break;
            case Screens.Menu:
                _menu.SetActive(false);
                break;
            case Screens.Game:
                _game.SetActive(false);
                break;
            case Screens.Tela_Load:
                _tela_Load.SetActive(false);
                break;
        }

        switch (newScreen)
        {
            case Screens.SplashScreen:
                _splashScreen.SetActive(true);
                cam.SetActive(true);
                break;
            case Screens.Menu:
                _menu.SetActive(true);
                cam.SetActive(true);
                break;
            case Screens.Game:
                _game.SetActive(true);
                cam.SetActive(false);
                break;
            case Screens.Tela_Load:
                _tela_Load.SetActive(true);
                break;
        }

        lastScreen = newScreen;
    }
}
