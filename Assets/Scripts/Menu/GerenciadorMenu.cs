﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GerenciadorMenu : MonoBehaviour
{

    [Header("Jogador")]
    public Text ouroText;
    public Text nivelText;
    public Text sliderXpText;
    public Image personagemSpriteRenderer;
    public Text Personagem_Nome;
    public Text Personagem_Descricao;
    public Text Personagem_Nivel;

    [Header("Bruxa")]
    public Text bruxaNivelText;
    public Text bruxaNivelTextFicha;
    public Text bruxaSliderXpText;

    [Header("Inventor")]
    public Text inventorNivelText;
    public Text inventorNivelTextFicha;
    public Text inventorSliderXpText;

    [Header("Pirata")]
    public Text pirataNivelText;
    public Text pirataNivelTextFicha;
    public Text pirataSliderXpText;

    [Header("Extras")]
    public GameObject npc;
    public DadosJogador dadosJogador;

    [SerializeField]
    Personagens personagens;

    int xpParaUpar;
    int xpParaUparBruxa;
    int xpParaUparInventor;
    int xpParaUparPirata;

    #region menus
    // public GameObject menuPrincipal;
    // public GameObject menuLoja;
    // public GameObject menuPerfil;
    public GameObject lojaInventor;
    public GameObject perfilInventor;
    public GameObject lojaBruxa;
    public GameObject perfilBruxa;
    public GameObject lojaPirata;
    public GameObject perfilPirata;

    public Animator anim;

    public bool menu_Principal;
    public bool menu_Loja;
    public bool menu_Perfil;
    #endregion

    #region sons
    public AudioSource source;
    [Space(20)]
    public AudioClip menuConfirmSnd;
    public AudioClip menuCancel;
    public AudioClip menuDenied;
    public AudioClip menuBuy;
    public AudioClip menuLvlUp;
    public AudioClip menuPause;
    public AudioClip menuUnPause;
    #endregion


    void Start()
    {
        //personagemSpriteRenderer = GameObject.Find("PersonagemDisplay").GetComponent<Image> ();
        //dadosJogador.jogadorXpAtual = 0;
        //dadosJogador.jogadorNivel = 1;
        source = GetComponent<AudioSource>();
        xpParaUpar = dadosJogador.jogadorXpMaximoDoNivelAtual;
        dadosJogador.SaveInfo();
        menu_Principal = true;
        menu_Perfil = false;
        menu_Loja = false;
        TrocarImagem(DadosJogador.personagemAtual);
    }
    void Update()
    {
        //dadosJogador.diamond++;
        //dadosJogador.ouro++;
        //dadosJogador.jogadorXpAtual++;

        ouroText.text = dadosJogador.ouro.ToString();
        nivelText.text = dadosJogador.jogadorNivel.ToString();
        sliderXpText.text = dadosJogador.jogadorXpAtual.ToString() + "/" + dadosJogador.jogadorXpMaximoDoNivelAtual.ToString() + "XP";

        AtualizaUIInfosPersonagens();
        AtualizaNivelJogadorPersonagens();
        //Debug.Log (diamond);

    }

    public void AtualizaUIInfosPersonagens()
    {

        if (bruxaNivelText.gameObject.activeInHierarchy)
            bruxaNivelText.text = dadosJogador.bruxaNivel.ToString();
        if (DadosJogador.personagemAtual == 1)
            Personagem_Nivel.text = "Lv. " + dadosJogador.bruxaNivel.ToString();
        if (bruxaNivelTextFicha.gameObject.activeInHierarchy)
            bruxaNivelTextFicha.text = dadosJogador.bruxaNivel.ToString();
        if (bruxaSliderXpText.gameObject.activeInHierarchy)
            bruxaSliderXpText.text = dadosJogador.bruxaXpAtual.ToString() + "/" + dadosJogador.bruxaXpMaximoDoNivelAtual.ToString() + "XP";

        if (inventorNivelText.gameObject.activeInHierarchy)
            inventorNivelText.text = dadosJogador.inventorNivel.ToString();
        if (DadosJogador.personagemAtual == 0)
            Personagem_Nivel.text = "Lv. " + dadosJogador.inventorNivel.ToString();
        if (inventorNivelTextFicha.gameObject.activeInHierarchy)
            inventorNivelTextFicha.text = dadosJogador.inventorNivel.ToString();
        if (inventorSliderXpText.gameObject.activeInHierarchy)
            inventorSliderXpText.text = dadosJogador.inventorXpAtual.ToString() + "/" + dadosJogador.inventorXpMaximoDoNivelAtual.ToString() + "XP";

        if (pirataNivelText.gameObject.activeInHierarchy)
            pirataNivelText.text = dadosJogador.pirataNivel.ToString();
        if (DadosJogador.personagemAtual == 2)
            Personagem_Nivel.text = "Lv. " + dadosJogador.pirataNivel.ToString();
        if (pirataNivelTextFicha.gameObject.activeInHierarchy)
            pirataNivelTextFicha.text = dadosJogador.pirataNivel.ToString();
        if (pirataSliderXpText.gameObject.activeInHierarchy)
            pirataSliderXpText.text = dadosJogador.pirataXpAtual.ToString() + "/" + dadosJogador.pirataXpMaximoDoNivelAtual.ToString() + "XP";
    }
    public void AtualizaNivelJogadorPersonagens()
    {

        if (dadosJogador.jogadorXpAtual >= dadosJogador.jogadorXpMaximoDoNivelAtual)
        {
            LevelUp();
        }

        if (dadosJogador.bruxaXpAtual >= dadosJogador.bruxaXpMaximoDoNivelAtual)
        {
            BruxaLevelUp();
        }

        if (dadosJogador.inventorXpAtual >= dadosJogador.inventorXpMaximoDoNivelAtual)
        {
            InventorLevelUp();
        }

        if (dadosJogador.pirataXpAtual >= dadosJogador.pirataXpMaximoDoNivelAtual)
        {
            PirataLevelUp();
        }
    }
    public void LevelUp()
    {
        //sobe 20% do xp necessario pra upar 
        xpParaUpar = (dadosJogador.jogadorXpMaximoDoNivelAtual * 2) + (int)(Mathf.RoundToInt(dadosJogador.jogadorXpMaximoDoNivelAtual * 0.3f));
        dadosJogador.jogadorVidaTotal = dadosJogador.jogadorVidaTotal + (int)(Mathf.RoundToInt(dadosJogador.jogadorVidaTotal * 0.15f));
        dadosJogador.jogadorDano = dadosJogador.jogadorDano + (int)(Mathf.RoundToInt(dadosJogador.jogadorDano * 0.1f));
        dadosJogador.ouroGanhoPorVitoria = dadosJogador.ouroGanhoPorVitoria + (int)(Mathf.RoundToInt(dadosJogador.ouroGanhoPorVitoria * 0.2f));
        dadosJogador.xpGanhoPorVitoria = (dadosJogador.xpGanhoPorVitoria * 2) + (int)(Mathf.RoundToInt(dadosJogador.xpGanhoPorVitoria * 0.2f));
        //sobe de nivel
        dadosJogador.jogadorNivel++;
        //ajusta o valor pro jogador passar pro proximo nivel
        dadosJogador.jogadorXpMaximoDoNivelAtual = xpParaUpar;
        dadosJogador.SaveInfo();
    }
    public void BruxaLevelUp()
    {
        xpParaUparBruxa = (dadosJogador.bruxaXpMaximoDoNivelAtual * 2) + (int)(Mathf.RoundToInt(dadosJogador.bruxaXpMaximoDoNivelAtual * 0.3f));
        dadosJogador.bruxaNivel++;
        dadosJogador.bruxaXpMaximoDoNivelAtual = xpParaUparBruxa;
        dadosJogador.SaveInfo();
    }
    public void InventorLevelUp()
    {
        xpParaUparInventor = (dadosJogador.inventorXpMaximoDoNivelAtual * 2) + (int)(Mathf.RoundToInt(dadosJogador.inventorXpMaximoDoNivelAtual * 0.3f));
        dadosJogador.inventorNivel++;
        dadosJogador.inventorXpMaximoDoNivelAtual = xpParaUparInventor;
        dadosJogador.SaveInfo();
    }

    public void PirataLevelUp()
    {
        xpParaUparPirata = (dadosJogador.pirataXpMaximoDoNivelAtual * 2) + (int)(Mathf.RoundToInt(dadosJogador.pirataXpMaximoDoNivelAtual * 0.3f));
        dadosJogador.pirataNivel++;
        dadosJogador.pirataXpMaximoDoNivelAtual = xpParaUparPirata;
        dadosJogador.SaveInfo();
    }
    private void TrocarImagem(int n)
    {
        personagemSpriteRenderer.sprite = personagens.personagens[n].Personagem_Imagem;
        Personagem_Descricao.text = personagens.personagens[n].Personagem_Descricao.text.ToString();
        Personagem_Nome.text = personagens.personagens[n].Personagem_Nome.text.ToString();
    }

    public void ClicaSetaDireita()
    {
        if (DadosJogador.personagemAtual < personagens.personagens.Length - 1)
        {
            DadosJogador.personagemAtual++;
        }
        else
        {
            DadosJogador.personagemAtual = 0;
        }
        TrocarImagem(DadosJogador.personagemAtual);
        
    }



    public void ClicaSetaEsquerda()
    {
        if (DadosJogador.personagemAtual > 0)
        {
            DadosJogador.personagemAtual--;
        }
        else
        {
            DadosJogador.personagemAtual = personagens.personagens.Length - 1;
        }
        TrocarImagem(DadosJogador.personagemAtual);
    }


    public void personagemLojaPopUp()
    {
        if (EventSystem.current.currentSelectedGameObject.name == "Inventor")
        {
            lojaInventor.SetActive(true);
            iTween.ScaleFrom(lojaInventor, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.2f, "easetype", iTween.EaseType.linear));
        }
        if (EventSystem.current.currentSelectedGameObject.name == "Bruxa")
        {
            lojaBruxa.SetActive(true);
            iTween.ScaleFrom(lojaBruxa, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.2f, "easetype", iTween.EaseType.linear));
        }
        if (EventSystem.current.currentSelectedGameObject.name == "Pirata")
        {
            lojaPirata.SetActive(true);
            iTween.ScaleFrom(lojaPirata, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.2f, "easetype", iTween.EaseType.linear));
        }
    }
    public void fechaPersonagemLojaPopUp()
    {
        if (EventSystem.current.currentSelectedGameObject.name == "Inventor")
        {
            iTween.ScaleTo(lojaInventor, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.15f, "easetype", iTween.EaseType.linear, "oncomplete", "fechaInventor", "oncompletetarget", this.gameObject));  
        }
        if (EventSystem.current.currentSelectedGameObject.name == "Bruxa")
        {
            iTween.ScaleTo(lojaBruxa, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.15f, "easetype", iTween.EaseType.linear, "oncomplete", "fechaBruxa", "oncompletetarget", this.gameObject));
        }
        if (EventSystem.current.currentSelectedGameObject.name == "Pirata")
        {
            iTween.ScaleTo(lojaPirata, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.15f, "easetype", iTween.EaseType.linear, "oncomplete", "fechaPirata", "oncompletetarget", this.gameObject));
        }
           
    }

    public void personagemPerfilPopUp()
    {
        if (EventSystem.current.currentSelectedGameObject.name == "Inventor")
        {
            perfilInventor.SetActive(true);
            iTween.ScaleFrom(perfilInventor, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.2f, "easetype", iTween.EaseType.linear));
        }
        if (EventSystem.current.currentSelectedGameObject.name == "Bruxa")
        {
            perfilBruxa.SetActive(true);
            iTween.ScaleFrom(perfilBruxa, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.2f, "easetype", iTween.EaseType.linear));
        }
        if (EventSystem.current.currentSelectedGameObject.name == "Pirata")
        {
            perfilPirata.SetActive(true);
            iTween.ScaleFrom(perfilPirata, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.2f, "easetype", iTween.EaseType.linear));
        }
    }
    public void fechaPersonagemPerfilPopUp()
    {
        if (EventSystem.current.currentSelectedGameObject.name == "Inventor")
        {
            iTween.ScaleTo(perfilInventor, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.15f, "easetype", iTween.EaseType.linear, "oncomplete", "fechaPerfilInventor", "oncompletetarget", this.gameObject));
        }
        if (EventSystem.current.currentSelectedGameObject.name == "Bruxa")
        {
            iTween.ScaleTo(perfilBruxa, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.15f, "easetype", iTween.EaseType.linear, "oncomplete", "fechaPerfilBruxa", "oncompletetarget", this.gameObject));
        }
        if (EventSystem.current.currentSelectedGameObject.name == "Pirata")
        {
            iTween.ScaleTo(perfilPirata, iTween.Hash("scale", Vector3.Scale(transform.localScale, Vector3.zero), "time", 0.15f, "easetype", iTween.EaseType.linear, "oncomplete", "fechaPerfilPirata", "oncompletetarget", this.gameObject));
        }
    }

    public void menuPerfilBttn()
    {
        if (menu_Loja)
            anim.SetTrigger("LojaParaPerfil");
        if (menu_Principal)
            anim.SetTrigger("MenuParaPerfil");
        menu_Perfil = true;
        menu_Loja = false;
        menu_Principal = false;
        source.PlayOneShot(menuPause, 1.0f);
    }
    public void menuLojaBttn()
    {
        if (menu_Perfil)
            anim.SetTrigger("PerfilParaLoja");
        if (menu_Principal)
            anim.SetTrigger("MenuParaLoja");
        menu_Loja = true;
        menu_Perfil = false;
        menu_Principal = false;
        source.PlayOneShot(menuPause, 1.0f);
    }
    public void VoltarMenuPrincipal()
    {
        if (menu_Perfil)
            anim.SetTrigger("PerfilParaMenu");
        if (menu_Loja)
            anim.SetTrigger("LojaParaMenu");
        menu_Principal = true;
        menu_Loja = false;
        menu_Perfil = false;
        source.PlayOneShot(menuPause, 1.0f);
        //source.PlayOneShot(menuUnPause, 1.0f);
    }

    public void JogarOffline()
    {
        source.PlayOneShot(menuBuy, 1.0f);
        ControladorTelas.ChangeScreen(ControladorTelas.Screens.Game);
        GameController.npc = npc;
        GameController.modo_Jogo = GameController.Modo_Jogo.Offline;
    }

    void fechaInventor()
    {
        lojaInventor.SetActive(false);
        lojaInventor.transform.localScale = new Vector3(1, 1, 1);
    }
    void fechaBruxa()
    {
        lojaBruxa.SetActive(false);
        lojaBruxa.transform.localScale = new Vector3(1, 1, 1);
    }
    void fechaPirata()
    {
        lojaPirata.SetActive(false);
        lojaPirata.transform.localScale = new Vector3(1, 1, 1);
    }

    void fechaPerfilInventor()
    {
        perfilInventor.SetActive(false);
        perfilInventor.transform.localScale = new Vector3(1, 1, 1);
    }
    void fechaPerfilBruxa()
    {
        perfilBruxa.SetActive(false);
        perfilBruxa.transform.localScale = new Vector3(1, 1, 1);
    }
    void fechaPerfilPirata()
    {
        perfilPirata.SetActive(false);
        perfilPirata.transform.localScale = new Vector3(1, 1, 1);
    }

}
