﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class RunasMenuController : MonoBehaviour {

    // Use this for initialization
    public int custoRuna;
    public int nivelRuna;

    public Text custoRunaVidaText;
    public Text nivelRunaVidaText;

    public Text custoRunaDanoText;
    public Text nivelRunaDanoText;
    public Text vidaTotalTelaRuna;
    public Text danoTotalTelaRuna;

    private DadosJogador dadosJogador;
    private GerenciadorMenu gerenciaMenu;
    void Awake()
    {
        dadosJogador = GameObject.Find("GerenciadorDoJogo").GetComponent<DadosJogador>();
        gerenciaMenu = GameObject.Find("Menu").GetComponent<GerenciadorMenu>();
        
    }
    private void OnEnable()
    {
        //custoRuna = PlayerPrefs.GetInt("custoRuna", 110);
        //nivelRuna = PlayerPrefs.GetInt("nivelRuna", 0);
    }
    private void OnDisable()
    {
       // PlayerPrefs.SetInt("custoRuna", custoRuna);
        //PlayerPrefs.SetInt("nivelRuna", nivelRuna);
    }
    // Update is called once per frame
    void Update ()
    {
        vidaTotalTelaRuna.text = "Vida Total: " + dadosJogador.jogadorVidaTotal.ToString();
        danoTotalTelaRuna.text = "Dano Total: " + dadosJogador.jogadorDano.ToString();
        
        if (this.gameObject.name == "RunaVida")
        {
            nivelRunaVidaText.text = "Runa Nível: " + this.nivelRuna.ToString();
            if (this.nivelRuna <= 3)
            {
                custoRunaVidaText.text = this.custoRuna.ToString();
            }
            else
            {
                custoRunaVidaText.text = "MAX";
            }
        }
        if (this.gameObject.name == "RunaDano")
        {
            nivelRunaDanoText.text = "Runa Nível: " + this.nivelRuna.ToString();
            if (this.nivelRuna <= 3)
            {
                custoRunaDanoText.text = this.custoRuna.ToString();
            }
            else
            {
                custoRunaDanoText.text = "MAX";
            }
        }
    }
    public void ComprarRuna()
    {
        if (dadosJogador.ouro >= this.custoRuna && this.nivelRuna <= 3)
        {
            //gerenciaMenu.source.PlayOneShot(gerenciaMenu.menuConfirmSnd);
            dadosJogador.ouro -= this.custoRuna;

            if (gameObject.name == "RunaVida")
            {
                if (this.nivelRuna == 0)
                {
                    dadosJogador.jogadorVidaTotal = dadosJogador.jogadorVidaTotal + (int)(Mathf.RoundToInt(dadosJogador.jogadorVidaTotal * 0.1f));
                    this.custoRuna = this.custoRuna + (int)(Mathf.RoundToInt(this.custoRuna * 0.5f));
                    this.nivelRuna++;
                }
                else if (this.nivelRuna == 1)
                {
                    dadosJogador.jogadorVidaTotal = dadosJogador.jogadorVidaTotal + (int)(Mathf.RoundToInt(dadosJogador.jogadorVidaTotal * 0.2f));
                    this.custoRuna = this.custoRuna + (int)(Mathf.RoundToInt(this.custoRuna * 0.5f));
                    this.nivelRuna++;
                }
                else if (this.nivelRuna == 2)
                {
                    dadosJogador.jogadorVidaTotal = dadosJogador.jogadorVidaTotal + (int)(Mathf.RoundToInt(dadosJogador.jogadorVidaTotal * 0.3f));
                    this.custoRuna = this.custoRuna + (int)(Mathf.RoundToInt(this.custoRuna * 0.5f));
                    this.nivelRuna++;
                }
                else if (this.nivelRuna == 3)
                {
                    dadosJogador.jogadorVidaTotal = dadosJogador.jogadorVidaTotal + (int)(Mathf.RoundToInt(dadosJogador.jogadorVidaTotal * 0.4f));
                    this.custoRuna = this.custoRuna + (int)(Mathf.RoundToInt(this.custoRuna * 0.5f));
                    this.nivelRuna++;
                }
            }
            if (gameObject.name == "RunaDano")
            {
                if (this.nivelRuna == 0)
                {
                    dadosJogador.jogadorDano = dadosJogador.jogadorDano + (int)(Mathf.RoundToInt(dadosJogador.jogadorDano * 0.2f));
                    this.custoRuna = this.custoRuna + (int)(Mathf.RoundToInt(this.custoRuna * 0.5f));
                    this.nivelRuna++;
                }
                else if (this.nivelRuna == 1)
                {
                    dadosJogador.jogadorDano = dadosJogador.jogadorDano + (int)(Mathf.RoundToInt(dadosJogador.jogadorDano * 0.25f));
                    this.custoRuna = this.custoRuna + (int)(Mathf.RoundToInt(this.custoRuna * 0.5f));
                    this.nivelRuna++;
                }
                else if (this.nivelRuna == 2)
                {
                    dadosJogador.jogadorDano = dadosJogador.jogadorDano + (int)(Mathf.RoundToInt(dadosJogador.jogadorDano * 0.3f));
                    this.custoRuna = this.custoRuna + (int)(Mathf.RoundToInt(this.custoRuna * 0.5f));
                    this.nivelRuna++;
                }
                else if (this.nivelRuna == 3)
                {
                    dadosJogador.jogadorDano = dadosJogador.jogadorDano + (int)(Mathf.RoundToInt(dadosJogador.jogadorDano * 0.35f));
                    this.custoRuna = this.custoRuna + (int)(Mathf.RoundToInt(this.custoRuna * 0.5f));
                    this.nivelRuna++;
                }
            }
        }
        else
        {
            Debug.Log("erro");
        }
            //gerenciaMenu.source.PlayOneShot(gerenciaMenu.menuDenied);
    }
}
