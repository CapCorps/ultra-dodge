﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hit : MonoBehaviour {

    [HideInInspector]
    public int dano;
    [HideInInspector]
    public float stun;

    void OnTriggerEnter(Collider hit)
    {
        if (this.gameObject.name == "Bala(Clone)")
        {
            hit.gameObject.SendMessage("Hit", dano, SendMessageOptions.DontRequireReceiver);
            hit.gameObject.SendMessage("Stun", stun, SendMessageOptions.DontRequireReceiver);
            gameObject.SetActive(false);
        }
    }
}
