﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjPool : MonoBehaviour {

    [SerializeField]
    GameObject prefab;
    [SerializeField]
    public int numberOfObjs = 6, dano;
    [SerializeField]
    public float stun = 0;
    private List<GameObject> objs;

    void Start () {
        GameController.obj_Destruir.Add(gameObject);

        objs = new List<GameObject>();
		for(int i = 0; i < numberOfObjs; i++) {
            GameObject obj;
            obj = Instantiate(prefab, transform);
            obj.SetActive (false);
			objs.Add (obj);
            if(obj.GetComponent<Hit>() != null)
            {
                obj.GetComponent<Hit>().dano = dano;
                obj.GetComponent<Hit>().stun = stun;
            }
            GameController.obj_Destruir.Add(obj);
        }
    }

	public GameObject FindObj(){
		int numberOfObjs = objs.Count;
		for(int i = 0; i < numberOfObjs; i++) {
			if(!objs [i].activeInHierarchy)
                return objs [i];
		}
        GameObject obj = Instantiate(prefab, transform);
        obj.SetActive (true);
		objs.Add (obj);
        if (obj.GetComponent<Hit>() != null)
        {
            obj.GetComponent<Hit>().dano = dano;
            obj.GetComponent<Hit>().stun = stun;
        }
        GameController.obj_Destruir.Add(obj);
        return obj;
	}
}
