﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raio_inventor : MonoBehaviour {

    [SerializeField]
    Transform start, end;
    [SerializeField]
    float speed = 5;

    [HideInInspector]
    public float dist = 6;

    [SerializeField]
    Transform raio;

    void Start()
    //void Iniciar(float distancia)
    {
        start.localPosition = Vector3.zero;
        end.localPosition   = new Vector3(0, 0, 0.5f);
        raio.localScale     = new Vector3(raio.localScale.x, raio.localScale.y, 0.1f);
        raio.localPosition  = new Vector3(raio.localPosition.x, raio.localPosition.y, 0);

        //dist = distancia;
    }

	void Update () {
        //distancia
        float distancia = Vector3.Distance(start.position, end.position);

        if (distancia < dist)
        {
            end.localPosition += new Vector3(0, 0, speed * Time.deltaTime);

            raio.localScale = new Vector3(raio.localScale.x, raio.localScale.y, distancia);
            raio.localPosition = new Vector3(raio.localPosition.x, raio.localPosition.y, distancia / 2);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
