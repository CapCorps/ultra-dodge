﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bruxa : MonoBehaviour {

    private GameController gameController;
    private Player player;
    #region Skill
    private EventTriggerFunctions skill;

    public GameObject bruxaPooling;
    private ObjPool objPool;

    private Mira mira;
    public float coolDown;
    #endregion

    void Start () {

        gameController = GetComponent<Player>().gameController;
        player = GetComponent<Player>();
        #region Preparando a Skill
        //obj pooling
        bruxaPooling = Instantiate(bruxaPooling, gameController.transform);
        objPool = bruxaPooling.GetComponent<ObjPool>();

        //receber mira
        mira = GetComponentInChildren<Mira>();

        //receber botão
        skill = gameController.buttonSkill;
        #endregion
    }

    void Update () {
		
	}

    void Skill()
    {
        mira.Atirar(objPool, coolDown, player.source, player.sound_Tiro);
    }
}
