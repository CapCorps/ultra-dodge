﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoboInventor : Photon.PunBehaviour, IPunObservable
{
    Transform inimigo;

    [SerializeField]
    ObjPool objPool;
    bool poolInstantiate = false;

    float speed = 5;

    #region Tiro
    public Transform bulletExit;
    public float coolDownBullet;
    private bool timeBullet = true;

    [SerializeField]
    float distancia_Atirar = 6;
    #endregion

    #region Som
    [HideInInspector]
    public AudioSource source;
    public AudioClip[] sound_Tiro;
    public AudioClip[] sound_Skil;
    #endregion

    void Start () {
        source = GetComponent<AudioSource>();

        if (!photonView.isMine)
        {
            gameObject.SetActive(false);
        }

        if (GameController.npc != null)
        {
            inimigo = GameController.npc.transform;
            source.PlayOneShot(sound_Skil[Random.Range(0, sound_Skil.Length)]);
        }
        else {
            gameObject.SetActive(false);
        }
    }
	
	void Update () {
        if (photonView.isMine && Vector3.Distance(transform.position, inimigo.position) < 6)
        {
            #region atirar
            if (timeBullet)
            {
                GameObject bullet = objPool.FindObj();
                bullet.SetActive(true);
                bullet.transform.position = bulletExit.position;
                bullet.transform.rotation = bulletExit.rotation;
                bullet.SendMessage("Iniciar", distancia_Atirar, SendMessageOptions.DontRequireReceiver);
                source.PlayOneShot(sound_Tiro[Random.Range(0, sound_Tiro.Length)]);
                timeBullet = false;
                StartCoroutine(CoolDownBullet());
            }
            #endregion
        }
    }

    public void Instaciar_ObjPool(Player player)
    {
        if (!poolInstantiate)
        {
            objPool = Instantiate(objPool, player.objPool.transform);

            poolInstantiate = true;
        }
    }

    IEnumerator CoolDownBullet()
    {
        yield return new WaitForSeconds(coolDownBullet);
        timeBullet = true;
    }

    public void Desativar_Robo() {
        photonView.RPC("Desligar_OBJ", PhotonTargets.All);
        gameObject.SetActive(false);
    }

    [PunRPC]
    void Desligar_OBJ() {
        if (!photonView.isMine)
        {
            gameObject.SetActive(false);
        }
    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(gameObject.activeInHierarchy);
        }
        else
        {
            // Network player, receive data
            gameObject.SetActive((bool)stream.ReceiveNext());
        }
    }
}