﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventor : Photon.PunBehaviour {

    private GameController gameController;

    private Player player;
    private IA ia;

    #region skill
    [SerializeField]
    GameObject roboPrefab;

    bool porRobo = false;
    bool recarregou = true;
    RoboInventor robo;
    EventTriggerFunctions skill;
    #endregion

    #region som
    [Space(20)]
    public AudioClip[] sound_Dano;
    public AudioClip[] sound_Tiro;
    public AudioClip[] sound_Skill;
    #endregion

    void Start() {

        if (CompareTag("NPC"))
        {
            ia = GetComponent<IA>();
            gameController = ia.gameController;

            //Mandando objPooling
            ia.robo = Instantiate(roboPrefab, gameController.transform);
            GameController.obj_Destruir.Add(ia.robo);

            //Enviar som para o npc
            ia.sound_Tiro = sound_Tiro;
            ia.sound_Dano = sound_Dano;
        }
        else {
            player = GetComponent<Player>();
            if (photonView.isMine)
            {
                gameController = player.gameController;
                Quaternion rot = Quaternion.Euler(0, 180, 0);

                if(GameController.modo_Jogo == GameController.Modo_Jogo.Online)
                {
                    robo = PhotonNetwork.Instantiate(roboPrefab.name, roboPrefab.transform.position, rot, 0).GetComponent<RoboInventor>();
                }
                else
                {
                    robo = Instantiate(roboPrefab, roboPrefab.transform.position, rot).GetComponent<RoboInventor>();
                }
                GameController.obj_Destruir.Add(robo.gameObject);
                robo.gameObject.SetActive(false);

                //Add função botão skill
                skill = gameController.buttonSkill;
            }

            //Enviar som para o player
            player.sound_Tiro = sound_Tiro;
            player.sound_Dano = sound_Dano;
        }
    }

    void Update()
    {
        if (robo != null && porRobo)
        {
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; ++i)
                {
                    Robo(Input.GetTouch(i).position);
                }
            }//*
            else if (Input.GetMouseButtonDown(0))
            {
                Robo(Input.mousePosition);
            }//*/
        }

        if (player.se_Mover && CompareTag("Player") && photonView != null && photonView.isMine &&
            skill.function == EventTriggerFunctions.Functions.Down && recarregou)
        {
            recarregou = false;
            if(Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; ++i)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        porRobo = true;
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                porRobo = true;
            }
        }
	}

    void Robo(Vector3 pos_Input) {

        robo.Instaciar_ObjPool(player);
        robo.gameObject.SetActive(true);

        Ray ray = Camera.main.ScreenPointToRay(pos_Input);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            robo.transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);
        }

        porRobo = false;
        StartCoroutine(TempTorre());
    }

    IEnumerator TempTorre()
    {
        yield return new WaitForSeconds(10);
        robo.Desativar_Robo();
        StartCoroutine(recarregarTorre());
    }
    IEnumerator recarregarTorre()
    {
        yield return new WaitForSeconds(5);
        recarregou = true;
    }
}
