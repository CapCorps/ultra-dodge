﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour {

    public bool isPaused;
    public GameObject pauseScreen;
    public void Pausa()
    {
        if (!isPaused)
        {
            isPaused = true;
            pauseScreen.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
            isPaused = false;
            pauseScreen.SetActive(false);
            
        }
    }
    public void VoltarParaOmenu(){
        Time.timeScale = 1;
        isPaused = false;
        ControladorTelas.ChangeScreen(ControladorTelas.Screens.Menu);
	}
}
