﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Player : Photon.PunBehaviour
{
    #region GameController
    [HideInInspector] public GameController gameController;
    DadosJogador dadosJogador;

    public ObjPool objPool;

    [HideInInspector]
    public bool se_Mover = true;
    int jogadores_Prontos = 0;
    #endregion

    #region NavMesh
    NavMeshAgent agent;
    Vector3 target;
    #endregion

    #region Vida
    [SerializeField]
    Slider vidaSlider_Prefab;
    Slider vidaSlider;
    [HideInInspector]
    public int vidaAtual;
    int vida;
    bool ja_avisou = false;
    #endregion

    #region mira
    Mira mira;
    public float coolDownBullet = 1;
    #endregion

    #region tiro
    EventTriggerFunctions tiro;
    #endregion

    public static Animator anim;

    #region Som
    [HideInInspector] public AudioClip[] sound_Tiro;
    [HideInInspector] public AudioClip[] sound_Dano;
    [HideInInspector] public AudioSource source;
    #endregion

    void Start()
    {
        GameController.obj_Destruir.Add(gameObject);
        #region receber componentes
        //Componetes que todos pegam
        source = GetComponent<AudioSource>();
        mira = GetComponentInChildren<Mira>();
        objPool = Instantiate(objPool.gameObject, Vector3.zero, Quaternion.identity).GetComponent<ObjPool>();
        GameController.obj_Destruir.Add(objPool.gameObject);

        //Componentes que apenas os locais podem
        if (photonView.isMine || GameController.modo_Jogo == GameController.Modo_Jogo.Offline)
        {
            se_Mover = true;
            dadosJogador = gameController.dadosJogador;

            agent = this.GetComponent<NavMeshAgent>();
            anim = GetComponent<Animator>();

            tiro = gameController.buttonTiro;

            //Conta para deixar a escala Z em 1
            float z = Mathf.Abs(transform.localScale.z) / transform.localScale.z;
            mira.transform.localScale = new Vector3(1, 1, z);
            mira.Rot_positivo = false;
        }
        else if(GameController.modo_Jogo == GameController.Modo_Jogo.Online)
        {
            GameController.npc = this.gameObject;
        }
        #endregion

        #region HUD
        //Receber elementos
        if (photonView.isMine)
        {
            CriarVida_Slide(dadosJogador.jogadorVidaTotal);

            photonView.RPC("MaxLife", PhotonTargets.AllBuffered, dadosJogador.jogadorVidaTotal);
        }
        else if(GameController.modo_Jogo == GameController.Modo_Jogo.Offline)
        {
            MaxLife(dadosJogador.jogadorVidaTotal);
        }
        #endregion
    }

    void Update()
    {
        //dadosJogador.jogadorXpAtual++;

        #region vida
        if (vida != vidaAtual)
        {
            vida = vidaAtual;
            StartCoroutine(Efeito_Barra_Vida(transform, vidaSlider, vida));
        }

        if ((vida <= 0 && photonView.isMine && !ja_avisou) || Input.GetKeyDown(KeyCode.Space))
        {
            gameController.FinalizarPartida();
            ja_avisou = true;
        }
        #endregion

        if (photonView.isMine && se_Mover)
        {
            ProcessInputs();
        }
    }

    void ProcessInputs() {
        anim.SetBool("Andar", agent.hasPath);

        if (tiro.function == EventTriggerFunctions.Functions.Down)
        {
            anim.SetBool("Atirar", true);
            if(GameController.modo_Jogo == GameController.Modo_Jogo.Online)
            {
                photonView.RPC("Atirar", PhotonTargets.All);
            }
            else
            {
                Atirar();
            }
            tiro.function = EventTriggerFunctions.Functions.Null;
            iTween.ScaleTo(tiro.gameObject, iTween.Hash("scale", new Vector3(1.1f, 1.1f, 0), "easetype", iTween.EaseType.linear, "time", 0.1f));
        }
        else if (!GameController.botoes_Apertados || Input.touchCount > GameController.Count_Events())
        {
            
            #region Mouse controller
            //*
            if (Input.GetMouseButton(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    target = hit.point;
                    agent.SetDestination(target);
                }
            }//*/
            #endregion

            #region Touch controller
            /*
            if (GameController.botoes_Apertados)
            {
                Ray ray = Camera.main.ScreenPointToRay(Posicao());
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    target = hit.point;
                    agent.SetDestination(target);
                }
            }
            else if (Input.touchCount > 0)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    target = hit.point;
                    agent.SetDestination(target);
                }

            }//*/
            #endregion
        }
    }

    Vector3 Posicao()
    {
        Vector3 p = Vector3.zero;

        for (int i = 0; i < Input.touchCount; ++i)
        {
            for (int j = 0; j <= GameController.events.Length; j++)
            {
                if(j >= GameController.events.Length)
                {
                    return Input.GetTouch(i).position;
                }
                else if(GameController.events[j].eventData != null && Input.GetTouch(i).fingerId == GameController.events[j].eventData.pointerId)
                {
                    j = GameController.events.Length + 1;
                }
            }
        }

        return p;
    }

    void CriarVida_Slide(int maxLife) {
        GameObject canvas = GameObject.FindObjectOfType<Canvas>().gameObject;
        vidaSlider = Instantiate(vidaSlider_Prefab, canvas.transform).GetComponent<Slider>();
        GameController.obj_Destruir.Add(vidaSlider.gameObject);

        //Setar valores
        vidaSlider.maxValue = maxLife;
        vidaSlider.value = vidaSlider.maxValue;
        vidaAtual = (int)vidaSlider.maxValue;
        vida = vidaAtual;

        vidaSlider.gameObject.SetActive(false);
    }

    [PunRPC]
    void Atirar() {
        mira.Atirar(objPool, coolDownBullet, source, sound_Tiro);
    }

    [PunRPC]
    void MaxLife(int maxLife)
    {
        if (!photonView.isMine)
        {
            CriarVida_Slide(maxLife);
        }
    }

    void FinalizarPartida() {
        if (photonView.isMine)
        {
            if (vida <= 0)
            {
                gameController.Perdeu();
            }
            else
            {
                gameController.Ganhou();
            }

            gameController.partidaAcabou = true;
            ja_avisou = false;
        }
    }

    void Hit(int dano)
    {
        photonView.RPC("Receber_Dano", PhotonTargets.AllBuffered, dano);
    }

    void Stun(float stun) {
        se_Mover = false;
        StartCoroutine(Tempo_Stun(stun));
    }

    [PunRPC]
    void Receber_Dano(int dano) {
        vidaAtual -= dano;
        anim.SetTrigger("Dano");
        source.PlayOneShot(sound_Dano[Random.Range(0, sound_Dano.Length)]);
    }

    public IEnumerator Tempo_Stun(float t) {
        yield return new WaitForSeconds(t);
        se_Mover = true;
    }

    public IEnumerator Efeito_Barra_Vida(Transform player, Slider vidaSlider, float novoValor)
    {
        vidaSlider.gameObject.SetActive(true);
        Vector3 barra_pos = player.position;
        barra_pos += player.TransformDirection(Vector3.forward * -1);
        vidaSlider.transform.position = Camera.main.WorldToScreenPoint(barra_pos);

        yield return new WaitForSeconds(0.3f);
        vidaSlider.value = novoValor;
        yield return new WaitForSeconds(1.5f);
        vidaSlider.gameObject.SetActive(false);
    }
}