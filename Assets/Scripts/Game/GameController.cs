﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public Text Status;
    public DadosJogador dadosJogador;
    public static GameController gameController;

    #region HUD
    [Space(10)]
    public Canvas canvas;
    public EventTriggerFunctions buttonSkill;
    public EventTriggerFunctions buttonTiro;
    public EventTriggerFunctions buttonPause;
    public EventTriggerFunctions buttonTrocarCam;
    public Text postGameText;
    public GameObject pause_Screen;
    #endregion

    #region Cameras
    [Space(10)]
    [SerializeField]
    private GameObject cameras;
    private Camera [] cans;
    #endregion

    #region Game
    [SerializeField]
    GameObject[] Cenarios;
    [SerializeField]
    Personagens personagens;

    public enum Modo_Jogo
    {
        Online, Offline
    }

    public static Modo_Jogo modo_Jogo = Modo_Jogo.Offline;
    private bool jogo_Iniciado;

    static int jogadores_Prontos = 0;
    public static GameObject player;
    public static GameObject npc;
    public static GameObject cenario;

    public static Transform Campo_Positivo;
    public static Transform Campo_Negativo;

    public static bool botoes_Apertados = false;
    public static EventTriggerFunctions[] events;
    private float t;
    #endregion

    #region pos Jogo
    private string resultado;
    private string finalMessage;

    [HideInInspector]
    public bool partidaAcabou;
    public static List<GameObject> obj_Destruir = new List<GameObject>();
    #endregion

    void Update()
    {
        #region Iniciar Jogo
        if(!jogo_Iniciado)
        {
            if(modo_Jogo == Modo_Jogo.Online)
            {
                if(PhotonNetwork.playerList.Length >= 1)
                {
                    IniciarJogo();
                    jogo_Iniciado = true;
                    buttonPause.gameObject.SetActive(false);
                }
            }
            else if (modo_Jogo == Modo_Jogo.Offline)
            {
                PhotonNetwork.offlineMode = true;
                IniciarJogo();
                jogo_Iniciado = true;
                buttonPause.gameObject.SetActive(true);
            }
        }
        #endregion

        #region Controles dos botões
        if (buttonTiro.state == EventTriggerFunctions.Functions.Null
        && buttonSkill.state == EventTriggerFunctions.Functions.Null
        && buttonPause.state == EventTriggerFunctions.Functions.Null
        && buttonTrocarCam.state == EventTriggerFunctions.Functions.Null)
        {
            botoes_Apertados = false;
        }
        else {
            botoes_Apertados = true;
            
        }

        if(buttonPause.state == EventTriggerFunctions.Functions.Enter && Time.time > t)
        {
            Pausa();
            buttonPause.state = EventTriggerFunctions.Functions.Null;
            t = Time.time + .1f;
            
        }

        if (buttonTrocarCam.state == EventTriggerFunctions.Functions.Enter && Time.time > t)
        {
            trocar_Camera();
            buttonTrocarCam.state = EventTriggerFunctions.Functions.Null;
            t = Time.time + .1f;
            iTween.ScaleTo(buttonTrocarCam.gameObject, iTween.Hash("scale", new Vector3(1.1f, 1.1f, 0), "easetype", iTween.EaseType.linear, "time", 0.1f));
        }
        #endregion

        #region Anular
        if (buttonTiro.state == EventTriggerFunctions.Functions.Exit)
        {
            buttonTiro.state = EventTriggerFunctions.Functions.Null;
            iTween.ScaleTo(buttonTiro.gameObject, iTween.Hash("scale", new Vector3(1, 1, 1), "easetype", iTween.EaseType.linear, "time", 0.1f));
        }
        if (buttonSkill.state == EventTriggerFunctions.Functions.Exit)
        {
            buttonSkill.state = EventTriggerFunctions.Functions.Null;
        }
        if (buttonPause.state == EventTriggerFunctions.Functions.Exit)
        {
            buttonPause.state = EventTriggerFunctions.Functions.Null;
        }
        if (buttonTrocarCam.state == EventTriggerFunctions.Functions.Exit)
        {
            buttonTrocarCam.state = EventTriggerFunctions.Functions.Null;
            iTween.ScaleTo(buttonTrocarCam.gameObject, iTween.Hash("scale", new Vector3(1, 1, 0), "easetype", iTween.EaseType.linear, "time", 0.1f));
        }
        #endregion
    }

    public void IniciarJogo()
    {
        gameController = this.GetComponent<GameController>();
        //dadosJogador = GameObject.Find("GerenciadorDeDados").GetComponent<DadosJogador>();
        #region Cenario
        if (modo_Jogo == Modo_Jogo.Offline)
        {
            cenario = Instantiate(Cenarios[Random.Range(0,Cenarios.Length)], Vector3.zero, Quaternion.identity, transform);
        }
        else if (modo_Jogo == Modo_Jogo.Online)
        {
            ExitGames.Client.Photon.Hashtable h = PhotonNetwork.room.CustomProperties;
            int i = (int)h["map"];
            cenario = Instantiate(Cenarios[i], Vector3.zero, Quaternion.identity, transform);
        }
        obj_Destruir.Add(cenario);
        Campo_Positivo = cenario.transform.Find("Campos/+");
        Campo_Negativo = cenario.transform.Find("Campos/-");
        #endregion

        #region player
        Transform Campo;
        if (!PhotonNetwork.isMasterClient)
        {
            Campo = Campo_Positivo;
        }
        else
        {
            Campo = Campo_Negativo;
        }

        if(modo_Jogo == Modo_Jogo.Online)
        {
            player = PhotonNetwork.Instantiate(personagens.personagens[DadosJogador.personagemAtual].Prefab.name, Campo.position, Campo.rotation, 0);
        }
        else
        {
            player = Instantiate(personagens.personagens[DadosJogador.personagemAtual].Prefab, Campo.position, Campo.rotation);
        }
        cameras.transform.rotation = Campo.rotation;
        player.GetComponent<Player>().gameController = GetComponent<GameController>();
        #endregion

        #region NPC
        /*if (modo_Jogo == Modo_Jogo.Offline)
        {
            npc = Instantiate(npc, Campo_Negativo.position, Campo_Negativo.rotation, transform);
            obj_Destruir.Add(npc);
            npc.GetComponent<IA>().gameController = GetComponent<GameController>();
        }*/
        #endregion

        #region Camera
        cans = new Camera[cameras.transform.childCount];
        for (int i = 0; i < cans.Length; i++)
        {
            cans[i] = cameras.transform.GetChild(i).GetComponent<Camera>();
            cans[i].gameObject.SetActive(false);
        }
        cans[DadosJogador.cameraAtual].gameObject.SetActive(true);
        #endregion

        events = new EventTriggerFunctions[4] { buttonPause, buttonSkill, buttonTiro, buttonTrocarCam };

        postGameText.gameObject.SetActive(false);
    }

    public void AcabarJogo()
    {
        foreach (GameObject obj in obj_Destruir)
        {
            Destroy(obj);
        }
        obj_Destruir = new List<GameObject>();
        jogo_Iniciado = false;
    }

    public static int Count_Events()
    {
        int count = 0;
        for (int i = 0; i < events.Length; i++)
        {
            if (events[i].state == EventTriggerFunctions.Functions.Enter)
            {
                count++;
            }
        }
        return count;
    }

    private void trocar_Camera() {
        cans[DadosJogador.cameraAtual].gameObject.SetActive(false);
        if (DadosJogador.cameraAtual < cans.Length - 1)
        {
            DadosJogador.cameraAtual++;
        }
        else
        {
            DadosJogador.cameraAtual = 0;
        }
        cans[DadosJogador.cameraAtual].gameObject.SetActive(true);
    }

    private void Pausa() {
        Time.timeScale = Mathf.Abs(Time.timeScale - 1);
        pause_Screen.SetActive(!pause_Screen.activeInHierarchy);
    }

    public void VoltarMenu() {
        Time.timeScale = 1;
        pause_Screen.SetActive(false);
        ControladorTelas.ChangeScreen(ControladorTelas.Screens.Menu);
        AcabarJogo();
    }

    public void FinalizarPartida() {
        PhotonNetwork.LeaveRoom();
        PhotonView photonView = PhotonView.Get(this);
        photonView.RPC("Avisar_Players", PhotonTargets.All);
    }

    [PunRPC]
    void Avisar_Players()
    {
        player.SendMessage("FinalizarPartida", SendMessageOptions.RequireReceiver);
    }

    public void Ganhou()
    {
        resultado = "Vitoria!\n";
        dadosJogador.jogadorXpAtual += dadosJogador.xpGanhoPorVitoria;
        if (DadosJogador.personagemAtual == 0)
            dadosJogador.inventorXpAtual += dadosJogador.xpGanhoPorVitoria;
        if (DadosJogador.personagemAtual == 1)
            dadosJogador.bruxaXpAtual += dadosJogador.xpGanhoPorVitoria;
        if (DadosJogador.personagemAtual == 2)
            dadosJogador.pirataXpAtual += dadosJogador.xpGanhoPorVitoria;
        dadosJogador.ouro += dadosJogador.ouroGanhoPorVitoria;
        finalMessage = resultado + "+" + dadosJogador.xpGanhoPorVitoria + " XP\n" + "+" + dadosJogador.ouroGanhoPorVitoria + " Ouro";
        postGameText.gameObject.SetActive(true);
        StartCoroutine(TypeText());
        dadosJogador.SaveInfo();
    }
    public void Perdeu()
    {
        resultado = "Derrota!\n";
        dadosJogador.jogadorXpAtual += (int)(Mathf.RoundToInt(dadosJogador.xpGanhoPorVitoria * 0.3f));
        if (DadosJogador.personagemAtual == 0)
            dadosJogador.inventorXpAtual += (int)(Mathf.RoundToInt(dadosJogador.xpGanhoPorVitoria * 0.3f));
        if (DadosJogador.personagemAtual == 1)
            dadosJogador.bruxaXpAtual += (int)(Mathf.RoundToInt(dadosJogador.xpGanhoPorVitoria * 0.3f));
        if (DadosJogador.personagemAtual == 2)
            dadosJogador.pirataXpAtual += (int)(Mathf.RoundToInt(dadosJogador.xpGanhoPorVitoria * 0.3f));
        dadosJogador.ouro += (int)(Mathf.RoundToInt(dadosJogador.ouroGanhoPorVitoria * 0.3f));
        int xpGanhoEssaPartida;
        xpGanhoEssaPartida = (int)(Mathf.RoundToInt(dadosJogador.xpGanhoPorVitoria * 0.3f));
        int goldGanhoEssaPartida = (int)(Mathf.RoundToInt(dadosJogador.ouroGanhoPorVitoria * 0.3f));
        finalMessage = resultado + "+" + xpGanhoEssaPartida + " XP\n" + "+" + goldGanhoEssaPartida + " Ouro";
        postGameText.gameObject.SetActive(true);
        StartCoroutine(TypeText());
        dadosJogador.SaveInfo();
    }
    IEnumerator TypeText()
    {
        float letterPause = 0.2f;
        //string message;
        //message = resultado + "+" + dadosJogador.xpGanhoPorVitoria + " XP\n" + "+" + dadosJogador.ouroGanhoPorVitoria + " Ouro";
        foreach (char letter in finalMessage.ToCharArray())
        {
            postGameText.text += letter;
            yield return 0;
            yield return new WaitForSeconds(letterPause);
        }
    }

}
