﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Personagem", menuName = "Personagens", order = 1)]
public class Personagens : ScriptableObject {
    public Personagem[] personagens;
}

[System.Serializable]
public class Personagem
{
    public GameObject Prefab;
    public Sprite Personagem_Imagem;
    public Text Personagem_Nome;
    public Text Personagem_Descricao;
}
