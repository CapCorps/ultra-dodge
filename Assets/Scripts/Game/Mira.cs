﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mira : Photon.PunBehaviour {

    public Transform bulletExit;
    private float tempoTiro;

    #region Mover mira
    private bool atirar = false;

    [HideInInspector]
    public bool Rot_positivo = false;
    [Space(15)]
    public float MaxRot;
    public float speedRotate = 10;
    public float coolDownMira;
    #endregion

    void Start () {
        transform.localPosition = new Vector3(transform.localPosition.x, 0.15f, transform.localPosition.z);
	}
	
	void Update () {
        if ((photonView != null && photonView.isMine) || GameController.modo_Jogo == GameController.Modo_Jogo.Offline)
        {
            if (Rot_positivo && !atirar)
            {
                transform.Rotate(Vector3.up * Time.deltaTime * speedRotate);
            }
            else if (!Rot_positivo && !atirar)
            {
                transform.Rotate(Vector3.down * Time.deltaTime * speedRotate);
            }

            //Verificar se a mira ultrapassou a rotação
            if (transform.localRotation.y >= MaxRot)
            {
                Rot_positivo = false;
            }
            else if (transform.localRotation.y <= -MaxRot)
            {
                Rot_positivo = true;
            }
        }
    }

    public void Atirar(ObjPool objPool, float t, AudioSource source, AudioClip[] som)
    {
        if (Time.time > tempoTiro)
        {
            Player.anim.SetTrigger("Atirar");
            GameObject bullet = objPool.FindObj();
            bullet.transform.position = bulletExit.position;
            bullet.transform.rotation = transform.rotation;
            bullet.SetActive(true);
            bullet.gameObject.SendMessage("Iniciar", SendMessageOptions.RequireReceiver);

            //source.PlayOneShot(som[Random.Range(0, som.Length)]);

            atirar = true;
            StartCoroutine(CoolDownMira());
            tempoTiro = Time.time + t;
        }
    }

    IEnumerator CoolDownMira()
    {
        yield return new WaitForSeconds(coolDownMira);
        atirar = false;
    }
}
