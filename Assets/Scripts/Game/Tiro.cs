﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tiro : MonoBehaviour {

    private Vector3 inicial;
    public float distancia;
    public float speed;

    void Update()
    {
        //distancia
        float dist = Vector3.Distance(inicial, transform.position);

        if (dist < distancia)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    void Iniciar()
    {
        gameObject.SetActive(true);
        inicial = transform.position;
    }
}
