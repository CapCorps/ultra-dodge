﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
public class IA : MonoBehaviour {

    #region GameController
    [HideInInspector] public GameController gameController;
    private DadosJogador dadosJogador;
    private ObjPool objPool;
    #endregion

    #region Mira
    public float coolDownBullet = 1;
    [SerializeField] private Transform mira;
    private Mira mirascr;
    #endregion

    #region burrice artificial
    //NavMesh
    private NavMeshAgent agent;
    private Vector3 destino;

    //probabilidades
    private int probAndar = 30;
    private int probAtirar = 40;
    private int probSkill = 20;
    private int probNada = 10;

    private int sorteioMax;

    private bool andando = false;
    #endregion

    #region Provisorio
    public GameObject robo;

    [HideInInspector]public bool skill = false;
    #endregion

    #region barra de vida
    [SerializeField]
    private Slider vidaSlider;

    [HideInInspector]
    public int vidaAtual;
    private int vida;
    #endregion

    #region som
    [HideInInspector] public AudioClip[] sound_Tiro;
    [HideInInspector] public AudioClip[] sound_Dano;
    [HideInInspector] public AudioSource source;
    #endregion

    void Start () {
        source = GetComponent<AudioSource>();

        #region Receber Componetes
        //Outros
        dadosJogador = gameController.dadosJogador;
        agent = this.GetComponent<NavMeshAgent>();
        objPool = GetComponent<ObjPool>();

        //Slider
        vidaSlider = Instantiate(vidaSlider.gameObject, gameController.canvas.transform).GetComponent<Slider>();

        //Mira
        mira = Instantiate(mira, transform).transform;
        mirascr = mira.GetComponent<Mira>();
        #endregion

        #region vida
        //receber valores
        vidaAtual = (int)(Mathf.RoundToInt(dadosJogador.jogadorVidaTotal * 1.3f));
        vida = vidaAtual;

        //Sliders
        vidaSlider.maxValue = vida;
        vidaSlider.value = vida;
        vidaSlider.gameObject.SetActive(false);
        #endregion

        #region Mira
        mira.transform.localScale = transform.localScale;
        #endregion

        robo.SetActive(false);

        #region sorteio
        sorteioMax = probAndar + probAtirar + probSkill + probNada + 1;
        Sortear();
        #endregion
    }


    void Update () {

        #region Vida
        if (vida != vidaAtual)
        {
            vida = vidaAtual;
            StartCoroutine(Efeito_Barra_Vida(transform, vidaSlider, vida));
        }
        else if (vida <= 0 && !gameController.partidaAcabou)
        {
            gameController.partidaAcabou = true;
            gameController.Ganhou();
        }
        #endregion

        #region Andar
        if (Vector3.Distance(transform.position, destino) < 0.5f && !
            agent.isStopped)
        {
            agent.Stop();
            Sortear();
        }
        #endregion

        #region Atirar
        Ray ray = new Ray(mira.position, mira.TransformDirection(Vector3.up));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 50))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                int a = Random.Range(0, 1000);
                if (a < probAtirar)
                {
                    mirascr.Atirar(objPool, coolDownBullet, source, sound_Tiro);
                }

            }
            else {
                Sortear();
            }
        }

        //Debug.DrawRay(mira.transform.position, mira.transform.TransformDirection(Vector3.up) * 50, Color.red);
        #endregion
    }

    private void Sortear() {
        int a = Random.Range(0, sorteioMax);
        if (a < probSkill + probAtirar && !skill)
        {
            Skill();
            skill = true;
            StartCoroutine(trocar(Random.Range(0, 5)));
        }
        else if (a < probSkill + probAtirar + probAndar)
        {
            //print("andar");
            Andar();
        }
        else if (a < probSkill + probAtirar + probAndar + probNada) {
            // print("nada");
            StartCoroutine(trocar(Random.Range(0, 5)));
        }
    }

    private void Andar() {
        destino = new Vector3(Random.Range(-3, 3), transform.position.y, Random.Range(1.5f, 6.5f));
        agent.Resume();
        agent.SetDestination(destino);
    }

    private void Skill()
    {
        robo.SetActive(true);
        Vector3 local = new Vector3(Random.Range(-3, 3), transform.position.y, Random.Range(1.5f, 3));
        robo.transform.position = local;

        StartCoroutine(TempTorre());
    }

    void Hit(int dano)
    {
        vidaAtual -= dano;
        print(sound_Dano.Length);
        source.PlayOneShot(sound_Dano[Random.Range(0, sound_Dano.Length)]);
    }

    public IEnumerator Efeito_Barra_Vida(Transform player, Slider vidaSlider, float novoValor)
    {
        vidaSlider.gameObject.SetActive(true);
        Vector3 barra_pos = player.position;
        barra_pos += player.TransformDirection(Vector3.forward * -1);
        vidaSlider.transform.position = Camera.main.WorldToScreenPoint(barra_pos);

        yield return new WaitForSeconds(0.3f);
        vidaSlider.value = novoValor;
        yield return new WaitForSeconds(1.5f);
        vidaSlider.gameObject.SetActive(false);
    }

    IEnumerator trocar(float temp) {
        yield return new WaitForSeconds(temp);
        Sortear();
    }
    IEnumerator TempTorre()
    {
        yield return new WaitForSeconds(10);
        robo.SetActive(false);
        StartCoroutine(recarregarTorre());
    }
    IEnumerator recarregarTorre()
    {
        yield return new WaitForSeconds(5);
        skill = false;
    }
}
