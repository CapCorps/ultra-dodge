﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventTriggerFunctions : EventTrigger
{
    public enum Functions {
        Null, Down, Enter, Exit
    }
    public Functions function = Functions.Null;
    public Functions state = Functions.Null;

    [HideInInspector]
    public PointerEventData eventData = null;

    public override void OnPointerUp(PointerEventData data)
    {
        function = Functions.Down;
    }
    public override void OnPointerEnter(PointerEventData data)
    {
        eventData = data;
        state = Functions.Enter;
    }
    public override void OnPointerExit(PointerEventData data)
    {
        eventData = null;
        state = Functions.Exit;
    }
}

