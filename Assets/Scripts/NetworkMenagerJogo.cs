﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkMenagerJogo : MonoBehaviour
{
    public Text Status;
    private bool podeInstanciar = false;

    public Camera cam;
    float Et;

    void Start()
    {
        ConnectToPhoton();
        //StartCoroutine (StartSever ());

    }

    IEnumerator StartSever()
    {
        while (!podeInstanciar)
        {
            yield return new WaitForFixedUpdate();
        }

        Status.text = "Instanciou";
        Status.color = Color.green;

        cam.gameObject.SetActive(false);
    }

    void Update()
    {
        RoomOptions opcoesDaSala = new RoomOptions() { IsVisible = false, MaxPlayers = 2, IsOpen = true };
        PhotonNetwork.JoinOrCreateRoom("sala", opcoesDaSala, TypedLobby.Default);
    }

    void OnJoinedLobby()
    {
        Status.text = "Está no Lobby";
        Status.color = Color.blue;
        PhotonNetwork.CreateRoom("sala");
        Status.text = "Criou a sala";
        Status.color = Color.yellow;
    }

    void OnJoinedRoom()
    {
        Status.text = "Entrou na sala";
        Status.color = Color.red;

        PhotonNetwork.Instantiate("Player1", new Vector3(0, 0, 0), Quaternion.identity, 0);
    }

    void ConnectToPhoton()
    {
        try
        {
            PhotonNetwork.ConnectUsingSettings("v1");
            Status.text = "Conectou";
            Status.color = Color.green;
        }
        catch (UnityException error)
        {
            Debug.Log(error);
        }
    }
}