﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public class DadosJogador : MonoBehaviour
{

    public static DadosJogador dadosJogador;

    [Header("Jogador")]
    public int ouro;
    public int ouroGanhoPorVitoria;
    public int xpGanhoPorVitoria;
    public int jogadorXpAtual;
    public int jogadorXpMaximoDoNivelAtual;
    public int jogadorNivel;
    public int jogadorVidaTotal;
    public int jogadorDano;

    [Header ("Bruxa")]
    public int bruxaXpAtual;
    public int bruxaXpMaximoDoNivelAtual;
    public int bruxaNivel;

    [Header("Inventor")]
    public int inventorXpAtual;
    public int inventorXpMaximoDoNivelAtual;
    public int inventorNivel;

    [Header("Pirata")]
    public int pirataXpAtual;
    public int pirataXpMaximoDoNivelAtual;
    public int pirataNivel;

    [Header("Extras")]
    public static int personagemAtual;
    public string cenarioAtual;
    public static int cameraAtual;
    public RunasMenuController runaVida;
    public RunasMenuController runaDano;

    void Awake()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
        runaVida = GameObject.Find("RunaVida").GetComponent<RunasMenuController>();
        runaDano = GameObject.Find("RunaDano").GetComponent<RunasMenuController>();
        /*ouro = PlayerPrefs.GetInt("ouro", 0);
        ouroGanhoPorVitoria = PlayerPrefs.GetInt("ouroGanho", 56);
        xpGanhoPorVitoria = PlayerPrefs.GetInt("xpGanho", 30);
        jogadorXpAtual = PlayerPrefs.GetInt("xp", 0);
        jogadorXpMaximoDoNivelAtual = PlayerPrefs.GetInt("xpmaximo", 150);
        jogadorNivel = PlayerPrefs.GetInt("nivel", 1);
        jogadorVidaTotal = PlayerPrefs.GetInt("vida", 100);
        jogadorDano = PlayerPrefs.GetInt("dano", 15);
        */
        personagemAtual = PlayerPrefs.GetInt("personagemAtual", 0);
        cenarioAtual = PlayerPrefs.GetString("cenarioAtual", "cenario1");
        cameraAtual = PlayerPrefs.GetInt("cameraAtual", 0);
        //Debug.Log(Application.persistentDataPath);

        Load();
    }

    public void SaveInfo()
    {
        /*
        PlayerPrefs.SetInt("ouro", ouro);
        PlayerPrefs.SetInt("ouroGanho", ouroGanhoPorVitoria);
        PlayerPrefs.SetInt("xp", jogadorXpAtual);
        PlayerPrefs.SetInt("xpGanho", xpGanhoPorVitoria);
        PlayerPrefs.SetInt("xpmaximo", jogadorXpMaximoDoNivelAtual);
        PlayerPrefs.SetInt("nivel", jogadorNivel);
        PlayerPrefs.SetInt("vida", jogadorVidaTotal);
        PlayerPrefs.SetInt("dano", jogadorDano);
        */
        Save();
        PlayerPrefs.SetInt("personagemAtual", personagemAtual);
        PlayerPrefs.SetString("cenarioAtual", cenarioAtual);
        PlayerPrefs.SetInt("cameraAtual", cameraAtual);
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData data = new PlayerData();
        data.ouro = ouro;
        data.ouroGanhoPorVitoria = ouroGanhoPorVitoria;
        data.xpGanhoPorVitoria = xpGanhoPorVitoria;
        data.jogadorXpAtual = jogadorXpAtual;
        data.jogadorXpMaximoDoNivelAtual = jogadorXpMaximoDoNivelAtual;
        data.jogadorNivel = jogadorNivel;
        data.jogadorVidaTotal = jogadorVidaTotal;
        data.jogadorDano = jogadorDano;

        data.custoRunaVida = runaVida.custoRuna;
        data.nivelRunaVida = runaVida.nivelRuna;

        data.custoRunaDano = runaDano.custoRuna;
        data.nivelRunaDano = runaDano.nivelRuna;

        data.bruxaXpAtual = bruxaXpAtual;
        data.bruxaXpMaximoDoNivelAtual = bruxaXpMaximoDoNivelAtual;
        data.bruxaNivel = bruxaNivel;

        data.inventorXpAtual = inventorXpAtual;
        data.inventorXpMaximoDoNivelAtual = inventorXpMaximoDoNivelAtual;
        data.inventorNivel = inventorNivel;

        data.pirataXpAtual = pirataXpAtual;
        data.pirataXpMaximoDoNivelAtual = pirataXpMaximoDoNivelAtual;
        data.pirataNivel = pirataNivel;

        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            ouro = data.ouro;
            ouroGanhoPorVitoria = data.ouroGanhoPorVitoria;
            xpGanhoPorVitoria = data.xpGanhoPorVitoria;
            jogadorXpAtual = data.jogadorXpAtual;
            jogadorXpMaximoDoNivelAtual = data.jogadorXpMaximoDoNivelAtual;
            jogadorNivel = data.jogadorNivel;
            jogadorVidaTotal = data.jogadorVidaTotal;
            jogadorDano = data.jogadorDano;

            runaVida.custoRuna = data.custoRunaVida;
            runaVida.nivelRuna = data.nivelRunaVida;

            runaDano.nivelRuna = data.nivelRunaDano;
            runaDano.custoRuna = data.custoRunaDano;

            inventorXpAtual = data.inventorXpAtual;
            inventorXpMaximoDoNivelAtual = data.inventorXpMaximoDoNivelAtual;
            inventorNivel = data.inventorNivel;

            bruxaXpAtual = data.bruxaXpAtual;
            bruxaXpMaximoDoNivelAtual = data.bruxaXpMaximoDoNivelAtual;
            bruxaNivel = data.bruxaNivel;

            pirataXpAtual = data.pirataXpAtual;
            pirataXpMaximoDoNivelAtual = data.pirataXpMaximoDoNivelAtual;
            pirataNivel = data.pirataNivel;
        }
    }
}
[Serializable]
class PlayerData
{
    public int ouro;
    public int ouroGanhoPorVitoria;
    public int xpGanhoPorVitoria;
    public int jogadorXpAtual;
    public int jogadorXpMaximoDoNivelAtual;
    public int jogadorNivel;
    public int personagemNivel;
    public int jogadorVidaTotal;
    public int jogadorDano;

    public int custoRunaVida;
    public int nivelRunaVida;

    public int custoRunaDano;
    public int nivelRunaDano;

    public int bruxaXpAtual;
    public int bruxaXpMaximoDoNivelAtual;
    public int bruxaNivel;
    public int inventorXpAtual;
    public int inventorXpMaximoDoNivelAtual;
    public int inventorNivel;
    public int pirataXpAtual;
    public int pirataXpMaximoDoNivelAtual;
    public int pirataNivel;
}
